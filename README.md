# Laravel Application

This is a boilerplate application for starting a new project with Laravel 6 and AdminLTE 3. 

#### Features Activated

- Fully configured with Laravel-Mix
- Activate plugins: DateTime Picker, Select2
- Totally untouched application structure, yet good to go with the styling
- Configured font family and font size, which is totally customizable

See `welcome.blade.php` for a kickstart.

**NOTE**: See Laravel-AdminLTE package documentation for more on configuration. However, if you decide not to use Laravel-Mix, you can disable the key from `config/adminlte.php`. And the resources will be loaded from within `public/vendor` directory. But this directory is not mandatory to keep while using with Laravel-Mix.
