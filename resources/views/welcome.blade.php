@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-md-2">
        <div class="input-group date" id="datetimepicker" data-target-input="nearest">
            <input type="text" name="from_date" class="form-control datetimepicker-input" data-target="#datetimepicker"
                required>
            <div class="input-group-append">
                <div class="input-group-text" data-target="#datetimepicker" data-toggle="datetimepicker"><i
                        class="fa fa-calendar-alt"></i></div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <select name="movie" id="movie" class="form-control">
                <option value="inception">Inception</option>
                <option value="interstellar">Interstellar</option>
                <option value="prestige">The Prestige</option>
                <option value="dark_knight">The Dark Knight</option>
            </select>
        </div>
    </div>
</div>
@stop

@section('js')
<script>
    $(function () {
        $('#movie').select2();
        $('#datetimepicker').datetimepicker({
            format: 'L',
            allowInputToggle: true
        });
    })

</script>
@stop
